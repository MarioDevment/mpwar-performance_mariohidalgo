<?php

declare(strict_types = 1);

namespace MarioDevment\Performance\Domain\Repository;

use MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image\ImageEntry;

interface FileRepository
{
    public function save(ImageEntry $fileEntry): void;
}
