<?php

declare(strict_types = 1);

namespace MarioDevment\Performance\Domain\ValueObject;

final class Uuid
{
    private $uuid;

    public function __construct(string $uuid)
    {
        $this->uuid = $uuid;
    }

    public function value(): string
    {
        return $this->uuid;
    }
}
