<?php

declare(strict_types = 1);

namespace MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image;

final class ImageName
{
    private $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function value(): string
    {
        return $this->name;
    }
}
