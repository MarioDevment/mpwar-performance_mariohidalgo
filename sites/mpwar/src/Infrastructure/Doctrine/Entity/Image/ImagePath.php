<?php

declare(strict_types = 1);

namespace MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image;

final class ImagePath
{
    private const PATH = 'upload/images/';
    private $path;

    public function __construct(string $path = self::PATH)
    {
        $this->path = $path;
    }

    public function value()
    {
        return self::PATH;
    }
}
