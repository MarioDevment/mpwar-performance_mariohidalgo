<?php

declare(strict_types = 1);

namespace MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image;

final class ImageFilter
{
    private $filter;

    public function __construct(string $filter)
    {
        $this->filter = $filter;
    }

    public function value(): string
    {
        return $this->filter;
    }
}
