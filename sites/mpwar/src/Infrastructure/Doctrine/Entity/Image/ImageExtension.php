<?php

declare(strict_types = 1);

namespace MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image;

final class ImageExtension
{
    private $extension;

    public function __construct(string $extension)
    {
        $this->extension = $extension;
    }

    public function value(): string
    {
        return $this->extension;
    }
}

