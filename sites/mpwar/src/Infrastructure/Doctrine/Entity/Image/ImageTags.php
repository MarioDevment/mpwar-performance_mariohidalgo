<?php

declare(strict_types = 1);

namespace MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image;

final class ImageTags
{
    private $tag;

    public function __construct(array $tag)
    {
        $this->tag = $tag;
    }

    public function addTag(string $tag): void
    {
        array_push($this->tag, $tag);
    }

    public function value(): array
    {
        return $this->tag;
    }
}
