<?php

declare(strict_types = 1);

namespace MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image\ImageScale;

final class Scale extends Type
{
    const NAME = 'scale';

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getGuidTypeDeclarationSQL($fieldDeclaration);
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ImageScale
    {
        $scale = new ImageScale((int) $value);
        return $scale;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): int
    {
        return $value->value();
    }

    public function getName()
    {
        return static::NAME;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}
