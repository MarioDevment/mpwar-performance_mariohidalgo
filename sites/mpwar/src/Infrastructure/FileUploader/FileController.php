<?php
declare(strict_types = 1);

namespace MarioDevment\Performance\Infrastructure\FileUploader;

use Doctrine\DBAL\Connection;
use MarioDevment\Performance\Application\FileUploader\FileBuilder;
use MarioDevment\Performance\Domain\ValueObject\Uuid;
use MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image\ImageEntry;
use MarioDevment\Performance\Infrastructure\EventDispatcher\Event;
use MarioDevment\Performance\Infrastructure\RabbitMQ\RabbitMQPublisher;
use MarioDevment\Performance\Infrastructure\Uuid\UuidGenerator;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class FileController extends Controller
{
    private $logger;
    private $connection;
    private $eventDispatcher;

    public function __construct(
        LoggerInterface $logger,
        Connection $connection,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->logger          = $logger;
        $this->connection      = $connection;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function upload(Request $request): Response
    {
        $image = $request->files->get('image');
        $uuid  = new Uuid(UuidGenerator::uuid4());

        $imageEntry = $this->uploadImage($image, $uuid);

        $this->publishToRabbitMQ($imageEntry);

        return new Response();
    }

    private function uploadImage($image, Uuid $uuid): ImageEntry
    {
        $imageUploadRepository = new FileUpload($image);
        $imageBuilder          = new FileBuilder($imageUploadRepository, $uuid);

        $imageEntry = $imageBuilder->uploadImage();

        return $imageEntry;
    }

    private function publishToRabbitMQ(ImageEntry $file): void
    {
        $container         = $this->container;
        $publishToRabbitMQ = new RabbitMQPublisher($container, $file);

        $this->eventDispatcher->dispatch(
            Event::whenTheImageUpload(),
            $publishToRabbitMQ
        );
    }
}
