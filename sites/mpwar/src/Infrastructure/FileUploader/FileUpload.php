<?php
declare(strict_types = 1);

namespace MarioDevment\Performance\Infrastructure\FileUploader;

use MarioDevment\Performance\Domain\Image\ImageUploadRepository;
use MarioDevment\Performance\Domain\ValueObject\Uuid;
use MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image\ImageClient;
use MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image\ImageDescription;
use MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image\ImageEntry;
use MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image\ImageExtension;
use MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image\ImageFilter;
use MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image\ImageName;
use MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image\ImagePath;
use MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image\ImageScale;
use MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image\ImageSize;
use MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image\ImageTags;
use MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image\ImageUuid;
use Symfony\Component\HttpFoundation\File\UploadedFile;

final class FileUpload implements ImageUploadRepository
{
    private $image;
    const ORIGINAL = 'original';

    public function __construct(UploadedFile $image)
    {
        $this->image = $image;
    }

    public function addFile(Uuid $uuid): ImageEntry
    {
        $imageEntry = $this->instanceImage($uuid);
        $this->moveImage($imageEntry);

        return $imageEntry;
    }

    private function moveImage(ImageEntry $imageEntry): void
    {
        $directory = $imageEntry->path()->value();
        $fullName  = $imageEntry->name()->value() . '.' . $imageEntry->ext()->value();
        $this->image->move($directory, $fullName);
    }

    private function instanceImage(Uuid $uuid): ImageEntry
    {
        $originalScale = 100;
        $unfiltered    = self::ORIGINAL;
        $clientName    = $this->image->getClientOriginalName();
        $extension     = $this->image->guessExtension();

        $md5Name     = md5(uniqid());
        $uuid4       = new ImageUuid($uuid->value());
        $name        = new ImageName($md5Name);
        $client      = new ImageClient($clientName);
        $extension   = new ImageExtension($extension);
        $path        = new ImagePath();
        $scala       = new ImageScale($originalScale);
        $filter      = new ImageFilter($unfiltered);
        $tags        = new ImageTags([$client->value(), $extension->value(), $filter->value()]);
        $description = new ImageDescription();

        $imageEntry = new ImageEntry($uuid4, $name, $client, $extension, $path, $scala, $filter, $tags, $description);

        return $imageEntry;
    }
}
