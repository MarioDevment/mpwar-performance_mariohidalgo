<?php

declare(strict_types = 1);

namespace MarioDevment\Performance\Infrastructure\Controller;

use Elasticsearch\ClientBuilder;
use MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image\ImageEntry;
use MarioDevment\Performance\Infrastructure\Doctrine\Repository\ImageRepository;
use MarioDevment\Performance\Infrastructure\Redis\RedisCache;
use Predis\Client;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

final class ImageViewController extends Controller
{
    private $redis;
    private const KEY = 'list_image';

    public function __construct(Client $redis)
    {
        $this->redis = $redis;
    }

    public function __invoke(Request $request)
    {
        $query  = $request->query->get('elastic');
        $redis  = new RedisCache($this->redis);
        $images = $this->userSearchRequest($query, $redis);

        return $this->render(
            'image.view.html.twig',
            [
                'images' => $images,
            ]
        );
    }

    public function save(Request $request): RedirectResponse
    {
        $uuid        = $request->request->get('uuid');
        $name        = $request->request->get('name');
        $description = $request->request->get('description');
        $tags        = $request->request->get('tags');

        $id = $this->persistOnDatabase($description, $uuid, $name, $tags);
        $this->persistOnRedis($id);

        return new RedirectResponse('/images');
    }

    private function getRepository(): ImageRepository
    {
        $repository = $this->getDoctrine()->getRepository(ImageEntry::class);

        return $repository;
    }

    private function checkImageOnRedis(RedisCache $redis): array
    {
        if ($redis->get(self::KEY)) {
            $listImages = $redis->get(self::KEY);
        } else {
            $listImages = $this->listImageFromDatabase();
            $redis->set(self::KEY, $listImages);
        }

        return json_decode($listImages);
    }

    private function listImageFromDatabase(): string
    {
        $repository = $this->getRepository();
        $images     = $repository->findAllId();
        $listImages = $this->array_flatten($images);

        return json_encode($listImages);
    }

    function array_flatten(array $array): array
    {
        $result = [];
        foreach ($array as $value) {
            if (is_array($value)) {
                $result = array_merge($result, $this->array_flatten($value));
            } else {
                array_push($result, $value);
            }
        }

        return $result;
    }

    private function getImages(array $listImages, RedisCache $redis): array
    {
        $images = [];
        foreach ($listImages as $imageId) {
            $id = (string) $imageId;

            if ($redis->get($id)) {
                $image = json_decode($redis->get($id), true);
            } else {
                $repository = $this->getRepository();
                $results    = $repository->find($id);
                $image      = $this->deserialize($results);
                $redis->set($id, json_encode($image));
            }

            $uuid = $image['uuid'];

            $images[$uuid][$id] = $image;
        }

        return $images;
    }

    private function persistOnDatabase(string $description, string $uuid, string $name, string $tags): string
    {
        $repository = $this->getRepository();

        $id = $repository->updateTagDescription($tags, $description, $uuid, $name);

        return $id;
    }

    private function persistOnRedis(string $id): void
    {
        $redisKey   = $id;
        $repository = $this->getRepository();

        $redis = new RedisCache($this->redis);
        $redis->invalidate($redisKey);

        $image = $repository->find($id);

        $json = $this->deserialize($image);
        $redis->set($redisKey, json_encode($json));
    }

    private function deserialize($image): array
    {
        $array = $image->jsonSerialize();

        return $array;
    }

    private function searchOnElastic($query): array
    {
        $params = [
            'index' => 'files',
            'body'  => [
                "query" => [
                    "query_string" => [
                        "query"                               => $query,
                        "auto_generate_synonyms_phrase_query" => false,
                    ],
                ],

            ],
        ];

        $client  = ClientBuilder::create()->build();
        $results = $client->search($params);

        $images = [];
        foreach ($results['hits']['hits'] as $image) {
            array_push($images, (int) $image['_id']);
        }

        return $images;
    }

    private function instanceElastic()
    {
        $indexParams['index'] = 'files';

        $client = ClientBuilder::create()->build();

        $params = [
            'index' => 'files',
        ];

        if (!$client->indices()->exists($indexParams)) {
            $client->indices()->create($params);
        }
    }

    private function userSearchRequest($query, $redis): array
    {
        if (!is_null($query)) {
            $this->instanceElastic();
            $listImages = $this->searchOnElastic($query);
            $images     = $this->getImages($listImages, $redis);
        } else {
            $listImages = $this->checkImageOnRedis($redis);
            $images     = $this->getImages($listImages, $redis);
        }

        return $images;
    }
}
