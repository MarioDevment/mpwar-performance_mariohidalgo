<?php

declare(strict_types = 1);

namespace MarioDevment\Performance\Infrastructure\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

final class MainController extends Controller
{
    public function __invoke()
    {
        return $this->render('index.html.twig');
    }
}
