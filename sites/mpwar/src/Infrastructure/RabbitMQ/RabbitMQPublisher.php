<?php

declare(strict_types = 1);

namespace MarioDevment\Performance\Infrastructure\RabbitMQ;

use MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image\ImageEntry;
use MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image\ImageFilter;
use MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image\ImageScale;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\Event;

final class RabbitMQPublisher extends Event
{
    private const PRODUCER      = 'old_sound_rabbit_mq.image_process_producer';
    private const GRAY_FILTER   = 'gray';
    private const NEGATE_FILTER = 'negate';
    private const KEY_SAVE_DB   = 'key_save_db';
    private const KEY_RESIZE    = 'key_resize';
    private const KEY_FILTER    = 'key_filter';
    private const CONTENT_TYPE  = 'application/json';
    private const ORIGINAL_SCALE = 100;
    private $image;
    private $container;

    public function __construct(ContainerInterface $container, ImageEntry $image)
    {
        $this->image     = $image;
        $this->container = $container;
    }

    public function queueToResizeOnRabbitMQ(): void
    {
        $this->scaleTo(85);
        $this->scaleTo(125);
        $this->scaleTo(150);
        $this->scaleTo(175);
        $this->scaleTo(200);
    }

    public function queueToFilterOnRabbitMQ(): void
    {
        $this->filterTo(self::GRAY_FILTER);
        $this->filterTo(self::NEGATE_FILTER);
    }

    public function queueSaveToDatabaseOnRabbitMQ(): void
    {
        $this->publish($this->image, self::KEY_SAVE_DB);
    }

    private function publish(ImageEntry $imageEntry, string $routeKey): void
    {
        $producer = $this->container->get(self::PRODUCER);
        $producer->setContentType(self::CONTENT_TYPE);

        $message       = $imageEntry->jsonSerialize();
        $rabbitMessage = json_encode($message);
        $producer->publish($rabbitMessage, $routeKey);
    }

    private function scaleTo(int $scale): void
    {
        $image      = $this->image;
        $imageScale = new ImageScale($scale);

        $image->changeScala($imageScale);
        $this->publish($image, self::KEY_RESIZE);
    }

    private function filterTo(string $filter): void
    {
        $image       = $this->image;
        $imageFilter = new ImageFilter($filter);
        $imageScale  = new ImageScale(self::ORIGINAL_SCALE);

        $image->changeScala($imageScale);
        $image->changeFilter($imageFilter);
        $this->publish($image, self::KEY_FILTER);
    }
}
