<?php
declare(strict_types = 1);

namespace MarioDevment\Performance\Infrastructure\Uuid;

use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Ramsey\Uuid\Uuid as UuidRamsey;

final class UuidGenerator
{
    static function uuid4(): string
    {
        try {
            $uuid4 = UuidRamsey::uuid4();

            return $uuid4->toString();
        } catch (UnsatisfiedDependencyException $e) {
            $optionalUuid = new UuidGenerator();

            return $optionalUuid->alternativeUuid4();
        }
    }

    private function alternativeUuid4(): string
    {
        $data = random_bytes(16);
        assert(strlen($data) == 16);

        $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }
}
