<?php

declare(strict_types = 1);

namespace MarioDevment\Performance\Infrastructure\EventDispatcher;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;
use MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image\ImageEntry;

class DoctrineSubscriber implements EventSubscriber
{
    public function getSubscribedEvents()
    {
        return [
            'postPersist',
            'postUpdate',
        ];
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof ImageEntry) {
            $this->handleEvent($entity);
        }
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $imageEntry = $args->getObject();
        $imageArray = $imageEntry->jsonSerialize();

        $elasticSearch = ClientBuilder::create()->build();

        $this->persistElasticSearch($imageArray, $elasticSearch);
    }

    private function persistElasticSearch(array $json, Client $elasticSearch): void
    {
        $params = [
            'index' => 'files',
            'type'  => 'image',
            'id'    => $json['id'],
            'body'  => [
                'tags'        => explode(',', $json['tags']),
                'filter'      => $json['filter'],
                'description' => $json['description'],
                'extension'   => $json['ext'],
                'name'        => $json['client'],
            ],
        ];

        $elasticSearch->index($params);
    }

    private function handleEvent(ImageEntry $image)
    {
        $elasticSearch = ClientBuilder::create()->build();
        $this->persistUpdateOnElasticSearch($image, $elasticSearch);
    }

    private function persistUpdateOnElasticSearch(ImageEntry $image, Client $elasticSearch): void
    {
        $tags   = implode(',', $image->tags()->value());
        $params = [
            'index' => 'files',
            'type'  => 'image',
            'id'    => $image->id(),
            'body'  => [
                'tags'        => $tags,
                'filter'      => $image->filter()->value(),
                'description' => $image->description()->value(),
                'extension'   => $image->ext()->value(),
                'name'        => $image->client()->value(),
            ],
        ];

        $elasticSearch->index($params);
    }
}
