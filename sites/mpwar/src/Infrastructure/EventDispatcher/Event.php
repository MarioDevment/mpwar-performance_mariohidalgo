<?php

declare(strict_types = 1);

namespace MarioDevment\Performance\Infrastructure\EventDispatcher;

final class Event
{
    private const IMAGE_UPLOAD = 'image.upload';
    private const IMAGE_RESIZE = 'image.changed';

    static function whenTheImageUpload()
    {
        return self::IMAGE_UPLOAD;
    }

    static function whenImageChanged()
    {
        return self::IMAGE_RESIZE;
    }
}
