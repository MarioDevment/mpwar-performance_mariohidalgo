import Dropzone from 'dropzone/dist/dropzone';
import swal from 'sweetalert';
import '../css/app.scss'

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})

let validateDropZone = true;
Dropzone.autoDiscover = false;

const imageDropzone = $("div#imagesUpload");
const urlImageUpload = $('.uploader').data('upload-image');

const maxFileSize = 30;
imageDropzone.dropzone({
    url: urlImageUpload,
    paramName: "image",
    method: "POST",
    maxFilesize: maxFileSize,
    clickable: true,
    acceptedFiles: 'image/*',
    addRemoveLinks: false,
    init: function () {
        this.on("uploadprogress", function (file, percent) {
            validateDropZone = percent === 100;
        });

        this.on("removedfile", function (file) {
            $.post(urlImageDelete, {name: file.name});
        });
    }
});
