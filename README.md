# Como ejecutar la aplicación

Descargar y ejecutar la siguiente maquina virtual.

## Desde un terminal:

- `git clone https://MarioDevment@bitbucket.org/MarioDevment/mpwar-performance_mariohidalgo.git`
- Verificar que los siguientes puertos estén disponibles, antes de iniciar la maquina:
    - 80, 443, 3306, 9200, 5601, 5672, 5673, 15672, 6379
- Ingresar a la carpeta de la practica `mpwar-performance_mariohidalgo`
- Ingresar a la carpeta vagrant `cd vagrant`
- Ejecutar `vagrant up` (Verificar si se presento un error durante la instalación)
- Ingresar por ssh `vagrant ssh`
- dentro de la maquina (192.168.50.50) ingresar a `cd /var/www/mpwar`
- ejecutar `composer update` (Recordar siempre hacerlo desde la maquina vagrant)

La maquina ya esta lista y configurada para funcionar, composer.json contiene los comandos necesarios para crear nuestra base de datos, schemes, colas en RabbitMQ e indices de elasticsearch, bastaría ingresar a la practica para probar su funcionamiento desde el siguiente enlace: [http://192.168.50.50](http://192.168.50.50 "Exercise Performance")

![Instalación](sites/mpwar/assets/images/wizzard/install.gif)

## Consumidores

Los consumidores no están configurados para ejecutarse automáticamente, por efectos prácticos.

Para ejecutar un consumidor, realice los siguientes pasos.

- ingresar a vagrant si aun no esta dentro: `vagrant ssh`
- `cd /var/www/mpwar`
- `php bin/console rabbitmq:multiple-consumer image_process`

![Consumers](sites/mpwar/assets/images/wizzard/consumers.gif)


## Video de la practica
[![Practica performance](sites/mpwar/assets/images/practicaSS.png)](https://youtu.be/WtOpjoPcENc?t=19s)

# Repositorio con codigo fuente

El código fuente original de la practica sin maquina virtual y con los commit del trabajo, están en la siguiente repositorio

- `git clone https://MarioDevment@bitbucket.org/MarioDevment/practica_performance.git`
- url: [https://bitbucket.org/MarioDevment/practica_performance/src/master/](https://bitbucket.org/MarioDevment/practica_performance/src/master/)

Para agregarlo en cualquier otra maquina, basta configurar el archivo **.env** y **.env.dist** con los parámetros de conexión para la db, rabbitmq, etc.

**En caso de instalar la practica en otra maquina, es importante tener Yarn 1.7.0 instalado**

## Otros datos

Usuario de RabitMQ
  - admin/admin

Usuario de MySQL
  - vagrant/vagrant
  - privatekey: **../vagrant/.vagrant/machines/web/virtualbox/private_key**

# Perfiles 

### Carga y búsqueda de imágenes
![Profile 1](sites/mpwar/assets/images/profile-1.png "Profile 1")
Se realiza el primer profile, el sistema solo aun no utiliza event dispatcher ni rabbitmq
- https://blackfire.io/profiles/09defc20-3ae9-44d8-90c0-eef6f83acfd5/graph

![Profile 2](sites/mpwar/assets/images/profile-2.png "Profile 2")
En este segundo profile, se implementan event dispatcher, encargados de enviar eventos al subscriber de RabbotMQ, el cual preparara las colas para que a las imágenes se les aplique filtro y resize, este evento activa otro event dispatcher, para administrar las colas de rabbitMQ encargadas de persistir las imágenes en base de dato.

No se realiza ninguna mejora, debido a que los componentes principales son de symfony, todo lo demás mantiene una arquitectura hexagonal y técnicas de código limpio.
- https://blackfire.io/profiles/ffb00a06-7d22-4b3c-b0cc-2ed1b8c61c2c/graph

![Profile 3](sites/mpwar/assets/images/profile-3.png "Profile 3")
El o los consumidor están listo y procesan la información de las colas y trabajan la imagen, la guardan en la database y lo persisten en redis.

El sistema, utiliza subscriber de Event dispatcher para comunicar a los eventos encargados de enviar los procesos a la cola de RabbitMQ, DropZone, por ahora el el único proceso que utiliza mayor recursos del necesario, pero por ser un librería de tercero, no se ejecuta ninguna ación para mejora.
Se mantienen buenas practicas de programación con SOLID.
- https://blackfire.io/profiles/01f0416f-213f-45e1-b6e6-4db97c38d37d/graph

### Vista de imágenes
![Profile 4](sites/mpwar/assets/images/profile-4.png "Profile 4") 
Este profile, pertenece a la vista de imágenes, carga las imágenes sin redis y sin mejoras
- https://blackfire.io/profiles/e65b4c2f-cf3d-4be6-b19f-590301f8075d/graph

![Profile 5](sites/mpwar/assets/images/profile-5.png "Profile 5") 
Seguimos sin redis, pero esta vez con una sobrecarga de imágenes.
- https://blackfire.io/profiles/1a93be26-ba80-465e-933e-795bdccebc0e/graph

![Profile 6](sites/mpwar/assets/images/profile-6.png "Profile 6") 
El siguiente profile, no muestra mayor cambio, aseguramos que las cargas las obtenga desde redis, en caso de no poseer la imagen, las crea en base a una lista y luego las obtiene, si ahi actualizaciones, las invalida para volver a listarlas.
- https://blackfire.io/profiles/c86fc24c-9177-4a6f-a4cf-1d1589a80dc0/graph

### APIS
![Profile 6](sites/mpwar/assets/images/profile-7.png "Profile 7") 
Se testearon los eventos del consumer de rabbit MQ, por medio de una api, no se encontro posibilidad de mejora, mas alla de procesos de bundles de terceros.

La practica desde un principio trato de llevarse con principios solid, pero debido a la utilización de symfony y bundles de terceros, mas allá del trabajo de refactoring constante, no hubo mayores cambio antes de la entrega final.
